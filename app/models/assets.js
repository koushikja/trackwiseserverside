
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let assetInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }

    },{strict:false},{collection: "asset"});

let asset = mongoose.model('asset',assetInfoSchema);
assetInfoSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
assetInfoSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
assetInfoSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});

module.exports = asset;