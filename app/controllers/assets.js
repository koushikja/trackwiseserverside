var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
var assetDataAccess = require("../dao").assets;

var _ = require("lodash");
module.exports = function (app){
    app.use('/', router);
};

router.post('/assetDetails', function(req, res, next) {
    var asset = req.body;

    assetDataAccess.saveAsset(asset)
        .then(function(result){
            res.send(result);
        }).catch(function(err){
        res.status(500).send(err)
    });
});


router.get('/getAllAssetCount', function(req, res, next) {
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    assetDataAccess.getCount(type)
        .then(function(assetCount){
            res.send({count:assetCount});
        })
        .catch(function(err){
            res.status(400).send({msg:err});
        })
});


//localhost:5100/asset/countByType


router.get('/lesserByLessorCode/:lessorCode', function(req, res, next) {
    assetModel.find({"LesserCode":req.params.lessorCode},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
});

//localhost:5100/asset/5b7d47ddd368c056a7e18549
router.get('/asset/:id', function(req, res, next) {
    var id = req.params.id;
    assetDataAccess.getAssetById(id)
        .then(function(allAssets){
            res.send(allAssets);
        })
});

//localhost:5100/assetsByLimit?skip=1?limit=2
router.get('/assetsByLimit', function(req, res, next) {

    let skip = req.query.skip;
    let limit = req.query.limit;
    let order = req.query.order;
    let type = req.query.type;
    if(type){
        type=_.split(type,",");
    }
    if(skip){
        skip = parseInt(skip)
    }
    if(limit){
        limit = parseInt(limit)
    }
    assetDataAccess.getAll(skip,limit,order,type)
        .then(function(allAssets){
            res.send(allAssets);
        })
});

let assetModel = require('../models/assets');


router.get('/assetsByAssetType/:assetType', function(req, res, next) {
    assetModel.find({"assetType":req.params.assetType},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
    
});


router.get('/assetsByAssetTypeByRange/:assetType/:start/:range', function(req, res, next) {
    assetModel.find({"assetType":req.params.assetType},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range));

})




router.get('/asset/countByAssetType/:assetType', function(req, res, next) {
    assetModel.count({"assetType":req.params.assetType},function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})


router.get('/asset/countByType', function(req, res, next) {
    assetDataAccess.getAllCountByTypes()
        .then(function(assetCount){
            res.send({count:assetCount});
        })
});


router.get('/getAssetDetailsById/:id', function(req, res, next) {
    assetModel.find({"_id":req.params.id},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
});


router.delete('/asset/:id', function(req, res, next) {
    var assetId = req.params.id;
    assetDataAccess.deleteAsset(assetId)
        .then(function(deletedAssets){
            res.send(deletedAssets);
        })
});


router.put('/asset/:id', function(req, res, next) {
    var assetObj = req.body;
    var id = req.params.id;
    assetDataAccess.update(id,assetObj)
        .then(function(updatedAssets){
            res.send(updatedAssets);
        })
});



router.get('/assetsByAssetForReport/:assetType', function(req, res, next) {
    assetModel.find({"assetType":req.params.assetType}, { _id: 0, __v: 0,'created':0,'updated':0,assetType:0},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/vechileAssetDetailsByVehicelId/:vehicleNumber', function(req, res, next) {
    assetModel.find({"vehicleNumber":req.params.vehicleNumber},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})
