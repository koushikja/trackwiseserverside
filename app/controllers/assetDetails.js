var express = require("express"),
  router = express.Router(),
  mongoose = require("mongoose"),
  assetDetailsModel = mongoose.model("asset");

module.exports = function(app) {
  app.use("/", router);
};

router.post("/addAssetDetails", function(req, res, next) {
  console.log("abcddsaf", req.body);
  var assetDetails = new assetDetailsModel(req.body);
  assetDetails.save(function(err, result) {
    if (err) {
      console.log("fuelDetailsPost failed: " + err);
    }
    res.send(result);
  });
});

router.get("/allAsset/count", function(req, res, next) {
  assetDetailsModel.count(function(err, userCount) {
    if (err) {
      res.status(500).send(err.message);
      logger.info(err.message);
    }
    var count = { count: userCount };
    res.send(count);
  });
});

router.get("/allAsset/:start/:range", function(req, res, next) {
  assetDetailsModel
    .find({}, function(err, result) {
      if (err) {
        res.send(err);
        console.log(err.stack);
      } else {
        res.send(result);
      }
    })
    .skip(parseInt(req.params.start))
    .limit(parseInt(req.params.range));
});

router.get("/assetById/:assetMongoId", function(req, res, next) {
  console.log("userMongoId", req.params.assetMongoId);
  assetDetailsModel.findOne({ _id: req.params.assetMongoId }, function(
    err,
    result
  ) {
    if (err) {
      console.log(err);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.post("/editAssetDetails/:assetMongoId", function(req, res, next) {
  assetDetailsModel.findOneAndUpdate(
    { _id: req.params.assetMongoId },
    req.body,
    { upsert: true, new: true },
    function(err, result) {
      if (err) {
        console.log(err.stack);
      } else {
        res.send(result);
      }
    }
  );
});

router.delete("/AssetByMongoId/:assetMongoId", function(req, res, next) {
  assetDetailsModel.remove({ _id: req.params.assetMongoId }, function(
    err,
    result
  ) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});
